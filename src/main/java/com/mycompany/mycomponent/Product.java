/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.mycomponent;

import java.util.ArrayList;

/**
 *
 * @author TAO
 */
public class Product {
    private int id;
    private String name;
    private double price;
    private String image;

    public Product(int id, String name, double price, String image) {
        this.id = id;
        this.name = name;
        this.price = price;
        this.image = image;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    @Override
    public String toString() {
        return "Product{" + "id=" + id + ", name=" + name + ", price=" + price + ", image=" + image + '}';
    }
    
    public static ArrayList<Product> getProductlist(){
        ArrayList<Product> list = new ArrayList<>();
        list.add(new Product(1,"Lattehot",40,"latteh.jpg"));
        list.add(new Product(1,"Latteice",45,"lattei.jpeg"));
        list.add(new Product(1,"Lattefrappe",50,"lattef.jpeg"));
        list.add(new Product(1,"Chocohot",40,"coh.jpg"));
        list.add(new Product(1,"Chocoice",45,"coi.jpg"));
        list.add(new Product(1,"Chocofrappe",50,"cof.jpg"));
        list.add(new Product(1,"Greenteahot",40,"greh.jpg"));
        list.add(new Product(1,"Greenteaice",45,"grei.jpg"));
        list.add(new Product(1,"Greenteafrappe",50,"gref.png"));
        list.add(new Product(1,"teahot",40,"teah.jpg"));
        return list;
    }
}
